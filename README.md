# PiScrape

## contents

- [Description](#description)
- [Resources](#resources)
- [Bootstrap ref](#bootstrap-ref)

# Description

This will eventually be a scraper to go out and find Raspberry Pi 4s that aren't sold out.

Right now it just scrapes workable.com for their front page job listings.

## Example of the results page

The results page loops over the directory containing page screenshots and creates a Bootstrap carousel, with the slide images determined in a forEach EJS loop:

![results page](results_page.png)

# Resources

- [How to speed up your Dockerfile with BuildKit cache mounts](https://vsupalov.com/buildkit-cache-mount-dockerfile/)
- [The final guide to web scraping with Node.js](https://geshan.com.np/blog/2021/09/web-scraping-nodejs/)
- [MDN: Arrow function expressions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)
- [How to capture screenshots with Puppeteer](https://dev.to/sagar/how-to-capture-screenshots-with-puppeteer-3mb2)
- [Async Await in Node.js - How to master it?](https://blog.risingstack.com/mastering-async-await-in-nodejs/)

# Bootstrap ref

`mt-#` = top margin

`ms-#` = left margin

`px-#` = padding left/right

`p-#` = padding
