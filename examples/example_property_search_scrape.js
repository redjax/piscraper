const puppeteer = require('puppeteer');
const axios = require('axios');
const cheerio = require('cheerio');

(async () => {
    
    const args = process.argv.slice(2)
    var zipcode = args[0] || 2000
    var excludeoffer = '1'
    const url = `https://www.domain.com.au/sale/?excludeunderoffer=${excludeoffer}&postcode=${zipcode}`


    try {
        const response = await axios.get(url)
        const $ = cheerio.load(response.data)
        const nOfProperties = $('h1>strong').text()
        console.log(`${nOfProperties} are open for rent in ${zipcode}`)
    } catch (e) {
        console.error(`Error while fetching rental properties for ${zipcode} - ${e.message}`)
    }
})()
