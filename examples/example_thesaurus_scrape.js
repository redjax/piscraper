// Tutorial: https://dev.to/code_jedi/web-scraping-in-nodejs-2lkf

const puppeteer = require('puppeteer')

async function scrape () {

    const browser = await puppeteer.launch({
        headless: true,
        args: [ '--use-gl=egl' ],
    })
    const page = await browser.newPage()

    await page.goto('https://www.thesaurus.com/browse/smart')

    // Get 5 synonyms
    for(i = 1; i < 6; i++) {

        // Right click element on page, inspect, then copy selector
        var htmlElement = "#meanings > div.css-ixatld.e15rdun50 > ul > li:nth-child(" + i + ") > a"
        var element = await page.waitForSelector(htmlElement)
        var text = await page.evaluate(element => element.textContent, element)

        console.log(text)

    }
    
    browser.close()

}

scrape()
