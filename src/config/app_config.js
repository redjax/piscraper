'use strict'

const path = require('path')
const fs = require('fs')

const logs = require('../libs/logger')
const log = logs.logger


const getNestedObject = (nestedObj, pathArr) => {
    // Function to search config objects for a configuration
    // i.e. an object in config.siteconfig like sitevars
    return pathArr.reduce((obj, key) =>
        (obj && obj[key] !== 'undefined') ? obj[key] : undefined, nestedObj);
}

// Set favicon path
const favicon_path = path.join('public', '/favicon/favicon.ico')

// Empty config array to store other configs
var config = {}

// Load site configuration. You need to create an app.json
//   file.
let appconf_file = path.join('config', 'confs/app.json')
log.debug(`Conf: appconf: file: ${appconf_file}`)
var appconf_import = JSON.parse(fs.readFileSync(appconf_file))
log.debug(`Conf: appconf: import: ${appconf_import}`)

if (!fs.existsSync(appconf_file)) {
    console.log(`Missing file: ${appconf_file}`)
    console.log(`Creating file: ${appconf_file}`)

    try {
        fs.openSync(appconf_file, 'w')
    } catch {
        console.log(`Could not create ${appconf_file}`)
    }
}

// Create config.appconf from imported file
config.appconf = appconf_import
// Create a "shortcut" to appconf nested objects
var appconf_site = config.appconf.sitevars

config.site_config = {
    port: process.env.NODE_CONTAINER_PORT,
    site_address: '0.0.0.0',
    favicon_path: favicon_path
}
log.debug(`Conf log: port: ${config.site_config.port}`)
log.debug(`Conf log: site_address: ${config.site_config.site_address}`)

config.sitevars = {
    'site_title': `${appconf_site.site_title}`,
    'pages_file': `${appconf_site.pages_file}`,
    'screenshot_dir': `${appconf_site.screenshot_dir}`
}

module.exports = {
    config,
    getNestedObject
}
