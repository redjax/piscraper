'use strict'

//
// IMPORTS
//

// Import winston functions, i.e. so you don't have to type winston.createLogger
const {createLogger, format, transports} = require('winston')
const morgan = require('morgan')
// const json = require('morgan-json')
const path = require('path')
// Import specific format functions, i.e. so you don't have to type format.timestamp
// const {combine, timestamp, json, colorize, align, printf, splat} = format;
const {combine, timestamp, json, printf} = format;
require('winston-daily-rotate-file')

//
// FILE DECLARATION
//

// App log file (all logs)
const log_file = path.join('logs', 'app.log')
// App error log file (all errors)
const error_log_file = path.join('logs', 'app-error.log')
// App warn log file (all warns)
const warn_log_file = path.join('logs', 'app-warn.log')
// App exception log file (all exceptions)
const exception_log_file = path.join('logs', 'app-exceptions.log')
// App rejection log file (all rejections)
const rejection_log_file = path.join('logs', 'app-rejections.log')
// Log rotate file
const rotate_log_file = path.join('logs', 'history/')

//
// DECLARATIONS
//

const fileRotateTransport = new transports.DailyRotateFile({
    filename: `${rotate_log_file + 'app-%DATE%.log'}`,
    datePattern: 'YYYY-MM-DD',
    maxFiles: '14d',
})


// Options dict, use like options.file.level
const options = {
    // Combined log file options
    file: {
        level: process.env.LOG_LEVEL || 'info',
        filename: log_file,
        handleExceptions: true,
        json: true,
        maxsize: 5242880,  // 5MB
        maxFiles: 5,
        colorize: false,
    },

    // Error log file options
    errorfile: {
        level: 'error',
        filename: error_log_file,
        handleExceptions: true,
        json: true,
        maxsize: 5242880,  // 5MB
        maxFiles: 5,
        colorize: false
    },

    // Warn log file options
    warnFile: {
        level: 'warn',
        filename: warn_log_file,
        handleExceptions: true,
        json: true,
        maxsize: 5242880,  // 5MB
        maxFiles: 5,
        colorize: false
    },

    // Exception log file options
    exceptionFile: {
        filename: exception_log_file
    },

    // Rejection log file options
    rejectionFile: {
        filename: rejection_log_file
    },

    // Console output options
    console: {
        level: process.env.LOG_LEVEL || 'info',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
}

//
// LOG FILTERS
//

// Create error filter for logs
const errorFilter = format((info) => {
    return info.level === 'error' ? info : false;
})
// Create info filter for logs
// const infoFilter = format((info) => {
//     return info.level === 'info' ? info : false;
// })
// Create warn filter for logs
const warnFilter = format((info) => {
    return info.level === 'warn' ? info : false;
})


//
// FUNCTIONS
//

// Initialize Winston logger
const logger = createLogger({

    // Don't crash the whole app on unhandled exception
    exitOnError: false,

    // Use docker .env LOG_LEVEL, default to info
    level: process.env.LOG_LEVEL || 'info',
    defaultMeta: {
        service: 'logging'
    },

    // Create log format
    format: combine(
        // i.e. 2022-04-10_03:24:06 PM
        timestamp({
            format: 'YYYY-MM-DD_hh:mm:ss A'
        }),
        // Output logs in "simple" format, i.e. not JSON
        format.simple(),
        // Format log print line
        printf((info) => `[${info.timestamp}] ${info.level} : ${info.message}`)
    ),

    transports: [

        fileRotateTransport,
        // app.log
        new transports.File({
            filename: options.file.filename,
            level: options.file.level,
        }),

        // app-error.log
        new transports.File({
            filename: options.errorfile.filename,
            level: options.errorfile.level,
            format: combine(
                errorFilter(),
                timestamp(),
                json(),
                format.simple(),
                printf((info) => `[${info.timestamp}] ${info.level} : ${info.message}`)
            )
        }),

        // app-warn.log
        new transports.File({
            filename: options.warnFile.filename,
            level: options.warnFile.level,
            format: combine(
                warnFilter(),
                timestamp(),
                json(),
                format.simple(),
                printf((info) => `[${info.timestamp}] ${info.level} : ${info.message}`)
            )
        }),

        // console logging
        new transports.Console({
            timestamp,
            level: options.console.level,
            format: format.combine(
                format.colorize(),
                timestamp({
                    format: 'YYYY-MM-DD_hh:mm:ss A'
                }),
                format.simple(),
                printf((info) => `[${info.timestamp}] ${info.level}: ${info.message}`)
            )
        })

    ],

    exceptionHandlers: [
        new transports.File({ filename: options.exceptionFile.filename})
    ],

    rejectionHandlers: [
        new transports.File({ filename: options.rejectionFile.filename})
    ]

})


// Create HTTP logger, for http server messages
const httpLogger = morgan("combined", format, {
    stream: {
        write: (message) => {
            const {
                method,
                url,
                status,
                contentLength,
                responseTime
            } = JSON.parse(message)

            logger.info('HTTP Access Log', {
                timestamp: new Date().toString(),
                method,
                url,
                status: Number(status),
                contentLength,
                responseTime: Number(responseTime)
            })
        }
    }
})

logger.info(`Debug level: ${process.env.LOG_LEVEL}`)

module.exports = {
    logger,
    httpLogger,
    opts: options
}