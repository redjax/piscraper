'use strict'

const fs = require('fs');
const path = require('path')

const logs = require('./logger')
const log = logs.logger
const hlog = logs.httpLogger


// Check if file/dir exists
const check_exists = (path) => {

    if (!fs.existsSync(path)) {
        log.debug(`Creating dir: ${path}`)

        fs.mkdirSync(path)
    } else {
        log.debug(`${path} exists.`)
    }

}


function move_to_history (scandir, subdir) {

    // Move files asynchronously
    async function move_async (scandir, subdir) {
        log.debug('move_to_history.move_files() async start')

        var move_from = `${scandir}/${subdir}`
        var move_to = `${move_from}/history/`

        let files = fs.readdirSync(move_from)
        console.log(`Files print: ${JSON.stringify(files)}`)

        log.debug(`move_from: ${move_from}, move_to: ${move_to}`)

        try {

            // Loop  files
            for (var file of files) {
                let from_path = path.join(`${move_from}`, file)
                let to_path = path.join(`${move_to}`, file)

                log.debug(`from_path: ${from_path}`)
                log.debug(`to_path: ${to_path}`)

                // Check if $file is file or dir
                var stat = fs.statSync(from_path)

                if (stat.isFile()) {
                    log.debug(`${from_path} is a file`)
                } else if (stat.isDirectory()) {
                    log.debug(`${from_path} is a directory`)
                }

                // Move file asynchronously
                fs.renameSync(from_path, to_path)
                log.debug(`Move ${from_path} to ${to_path}`)
            }
        } catch (error) {
            log.error(`Error: ${error}`)
        }
    }

    move_async(scandir, subdir)
}

function read_files(dir) {
    const files_output = {}

    const files = fs.readdirSync(dir)

    files.forEach(file => {
        var filetype = path.extname(file).replace('.','')
        var cleanName = file.substring(0, file.lastIndexOf("."))
        var cleanDir = dir.replace('./public/', '')

        log.debug(`Clean name: ${cleanName}, Dir: ${dir}, File: ${file}`)
        files_output[cleanName] = {
            "filetype": filetype,
            "cleanName": cleanName,
            "cleanDir": cleanDir,
            "filename": file,
            "dir": dir,
            "cleanPath": `${cleanDir}/${file}`,
            "path": `${dir}/${file}`,
        }
    })

    return files_output
}

function prepare_screenshots_for_page(screenshots_dir) {
    log.debug(`screenshots_dir: ${screenshots_dir}`)

    const screenshots_return = {}

    const dirs = fs.readdirSync(screenshots_dir)
    log.debug(`Dirs in ${screenshots_dir}: ${JSON.stringify(dirs)}`)


    dirs.forEach(dir => {
        log.debug(`Dir: ${dir}`)
        log.debug(`dirs.forEach screenshots_dir: ${screenshots_dir}`)

        const loop_dir = path.join(screenshots_dir, dir)

        log.debug(`Combined path: ${loop_dir}`)

        const files = fs.readdirSync(loop_dir)
        files.forEach(file => {
            log.debug(`File: ${file}`)

            var filetype = path.extname(file).replace('.','')
            log.debug(`filetype: ${filetype}`)
            var cleanName = file.substring(0, file.lastIndexOf("."))
            log.debug(`cleanName: ${cleanName}`)
            
            log.debug(`loop_dir: ${loop_dir}`)
            var filepath = loop_dir + "/" + file
            log.debug(`filepath: ${filepath}`)
            var cleanDir = loop_dir.replace('public/', '')
            log.debug(`cleanDir: ${cleanDir}`)

            // Check if $file is file or dir
            var stat = fs.statSync(filepath)

            if (stat.isFile()) {

                log.debug(`${filepath} is a file`)

                screenshots_return[file] = {
                    "subdir": dir,
                    "filename": file,
                    "fileType": filetype,
                    "cleanName": cleanName,
                    "cleanDir": cleanDir,
                    "path": filepath,
                    "cleanPath": `${cleanDir}/${file}`,
                    "html_id": `i-${file}`
                }
                log.debug('')
                log.debug(
                    `screenshots_return: subdir: ${screenshots_return[file].subdir}`,
                    `screenshots_return: filename: ${screenshots_return[file].filename}`,
                    `screenshots_return: fileType: ${screenshots_return[file].fileType}`,
                    `screenshots_return: cleanName: ${screenshots_return[file].cleanName}`,
                    `screenshots_return: cleanDir: ${screenshots_return[file].cleanDir}`,
                    `screenshots_return: cleanPath: ${screenshots_return[file].cleanPath}`,
                    `screenshots_return: path: ${screenshots_return[file].path}`,
                    `screenshots_return: html_id: ${screenshots_return[file].html_id}`
                )
                log.debug('')

            } else if (stat.isDirectory()) {

                log.debug(`${filepath} is a directory, skipping.`)

            }
        })
    })

    return screenshots_return

}



module.exports = {
    check_exists,
    move_to_history,
    read_files,
    prepare_screenshots_for_page
}



/////////////////////////////
// Move files asynchronously
// function move_to_history (scandir, subdir) {

//     // Move files asynchronously
//     async function move_async (scandir, subdir) {
//         log.debug('move_to_history.move_files() async start')

//         var move_from = `${scandir}/${subdir}`
//         var move_to = `${move_from}/history/`

//         let files = fs.readdirSync(move_from)
//         console.log(`Files print: ${JSON.stringify(files)}`)

//         log.debug(`move_from: ${move_from}, move_to: ${move_to}`)
        
//         try {

//             // Loop  files
//             for (var file of files) {
//                 let from_path = path.join(`${move_from}`, file)
//                 let to_path = path.join(`${move_to}`, file)

//                 log.debug(`from_path: ${from_path}`)
//                 log.debug(`to_path: ${to_path}`)

//                 // Check if $file is file or dir
//                 var stat = fs.statSync(from_path)

//                 if (stat.isFile()) {
//                     log.debug(`${from_path} is a file`)
//                 } else if (stat.isDirectory()) {
//                     log.debug(`${from_path} is a directory`)
//                 }

//                 // Move file asynchronously
//                 fs.renameSync(from_path, to_path)
//                 log.debug(`Move ${from_path} to ${to_path}`)
//             }
//         } catch (error) {
//             log.error(`Error: ${error}`)
//         }
//     }

//     move_async(scandir, subdir)
// }