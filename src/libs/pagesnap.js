'use strict'

const { time } = require('console');
const fs = require('fs');
const path = require('path')
const puppeteer = require('puppeteer');

const logs = require('./logger')
const log = logs.logger
const hlog = logs.httpLogger

const fileops = require('./fileops')
const appadmin = require('./app-admin')


// Create screenshot async function
const take_screen = (async function (pages, screenshot_dir = "screenshots") {
    // If screenshot_dir !exist, create
    log.debug(`Check ${screenshot_dir} exists`)
    fileops.check_exists(screenshot_dir)

    log.debug(' Initializing browser object')
    // Create browser object
    let browser = null

    try {

        log.debug(' Launch puppeteer')
        // Launch headless puppeteer
        browser = await puppeteer.launch({
            headless: true,
            args: ['--no-sandbox']
        })

        log.debug(' Open new page')
        // Open new page in browser
        const page = await browser.newPage()

        log.debug(' Set viewport')
        // Set page viewport
        await page.setViewport({
            width: 1440,
            height: 1080
        })

        // Loop over pages in passed json obj
        for (const { id, name, url } of pages) {

            log.debug(` id: ${id}, name: ${name}, url: ${url} `)

            let screenshot_subdir = `${screenshot_dir}/${name}`
            let history_dir = `${screenshot_subdir}/history`
            let screenshot_file = `${screenshot_dir}/${name}/${name}_${appadmin.timestamp()}.jpeg`
            log.debug(` screenshot_subdir: ${screenshot_subdir}, history_dir: ${history_dir}, screenshot_file: ${screenshot_file}`)

            log.debug(` Check if ${screenshot_subdir} exists`)
            // Check if screenshot subdir exist
            fileops.check_exists(`${screenshot_subdir}`)

            fileops.check_exists(`${history_dir}`)

            // Move previous scrape to subdir/history/old_screenshot.jpeg
            log.debug(` Pre move_to_history/screenshot_subddir: ${screenshot_subdir}`)
            fileops.move_to_history(screenshot_dir, name)

            log.debug(` Load ${url}`)
            // Load page
            await page.goto(url)

            log.debug(' Screenshot page')
            // Screenshot page
            await page.screenshot({ path: `${screenshot_file}` })

            log.info('Screenshot saved')
            log.debug(` Screenshot: ${name} - ${url}, saved to ${screenshot_dir}`)

        }

    } catch (e) {

        log.error(` ${e.message}`)

    } finally {
        // If browser is still open
        if (browser) {
            log.debug(` Closing browser`)
            // Close browser
            await browser.close()
        }

        log.info(`${pages.length} screenshots captured`)
    }
})


// Export functions
module.exports = {
    take_screen
}
