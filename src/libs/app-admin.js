'use strict'

const logs = require('./logger')
const log = logs.logger
const hlog = logs.httpLogger


function timestamp() {
    // Create .now() timestamp
    let now = new Date();

    // Parse mm, dd, yy, hh, mm, ss to vars
    const [month, day, year] = [now.getMonth(), now.getDate(), now.getFullYear()];
    const [hour, minute, second] = [now.getHours(), now.getMinutes(), now.getSeconds()];

    // Create timestamp string
    var timestamp = `${year}-${month}-${day}_${hour}-${minute}-${second}`

    log.debug(` ${timestamp}`)
    return timestamp
}

module.exports = {
    timestamp
}