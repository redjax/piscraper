const puppeteer = require('puppeteer');

(async function()  {

    try {

      const browser = await puppeteer.launch({
          // https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md#tips
          //   Set --disable-dev-shm-usage if running in docker
          args: ['--disable-dev-shm-usage']
      });
      const page = await browser.newPage();
      const navigationPromise = page.waitForNavigation();
  
      await page.goto('https://jobs.workable.com/');
      await page.setViewport({ width: 1440, height: 744 });
      await navigationPromise;
  
      const selector = "ul li h3 strong a"
      await page.waitForSelector(`${selector}`);

      let jobTitles = await page.$$eval(`${selector}`, function(titles) {
        
        const innertext = titles.map(function(title) {
          title.innerText
        });

        console.log(innertext)
        return innertext
        // return titles.map(function(title) {
        //     title.innerText
        // });
      });

      console.log(`Job Titles on first page of Workable are: ${jobTitles.join(', ')}`);

      await browser.close();

    } catch (e) {

      console.log(`Error while fetching workable job titles ${e.message}`);
    
    }

  })();
