'use strict'

const puppeteer = require('puppeteer');
const express = require('express')
const favicon = require('serve-favicon')
const fs = require('fs')

// My libs
const pagesnap = require('./libs/pagesnap')
const logs = require('./libs/logger')
const log = logs.logger.child({ service: 'app-main' })
const hlog = logs.httpLogger
const fileops = require('./libs/fileops')

// Set app config file
var config_lib = require('./config/app_config');
var config = config_lib.config

const sitevars = config.sitevars
const siteconfig = config.site_config

// Load pages.json, parse to JSON object
var pages_raw = fs.readFileSync('config/confs/pages.json')
var pages = JSON.parse(pages_raw)

log.debug(`App config print: ${JSON.stringify(config)}`)
log.debug(`App sitevars print: ${JSON.stringify(sitevars)}`)

var favicon_path = siteconfig.favicon_path

const app = express()
app.use(hlog)

// const port = process.env.NODE_PORT
// const host = config.site_config.host
const port = siteconfig.port
const host = siteconfig.site_address

log.debug(`Port: ${port}`)
log.debug(`Host: ${host}`)

const view_engine = 'ejs'
log.debug(`Setting view engine to ${view_engine}`)
app.set('view engine', view_engine)

// const static_dir = express.static('public')
// log.debug(`Static dir: ${static_dir}`)

const router = express.Router()

// Routes

// index
router.get('/', (req, res, next) => {

    // Render pages/index.ejs
    res.status(200).render('pages/index', {
        sitevars
    })
})

// initiate scrape
router.get('/scrape', (req, res, next) => {

    // Set dir to save screenshots
    var screenshot_dir = "page_screenshots"
    var screenshot_dir = sitevars.screenshot_dir
    log.debug(`Screenshot dir: ${JSON.stringify(screenshot_dir)}`)

    log.info('Taking screenshot')
    // Send pages.json for screenshots
    pagesnap.take_screen(pages, screenshot_dir)

    // redirect to home page while scrape runs
    res.status(200).redirect('/results')

})

router.get('/results', (req, res, next) => {
    
    // VARS to pass to page
    var screenshot_dir = sitevars.screenshot_dir || "page_screenshots"
    log.debug(`Screenshot dir: ${screenshot_dir}`)

    log.info(`TEST PAGE`)

    const load_screenshots = fileops.prepare_screenshots_for_page(screenshot_dir)
    const screenshots = load_screenshots

    log.debug(`Screenshot dictionary: ${JSON.stringify(screenshots)}`)

    res.status(200).render('pages/results', {
        sitevars,
        screenshots
    })

})

log.debug(`Setup route for /`)
app.use('/', router)
log.debug(`Set static dir`)
app.use(express.static('public'))
app.use('/static', express.static('public'))
app.use('/results', router)

log.debug('Setup favicon path')
app.use(favicon(favicon_path))

log.debug(`Set logging, error handlers`)
app.use(logErrors)
app.use(errorHandler)

function logErrors(err, req, res, next) {
    log.error(err.stack)
    next(err)
}

function errorHandler(err, req, res, next) {
    res.status(500).send('Error 500')
}

// Only print debug info to console if in dev environment
if (process.env.NODE_ENV !== 'production') {
    log.info(`NODE_ENV: ${process.env.NODE_ENV}`)
    log.debug(`.env host: ${host}`)
    log.debug(`.env port: ${port}`)
    log.debug(`view engine: ${view_engine}`)
    log.debug(`sitevars: ${JSON.stringify(sitevars)}`)
    log.debug(`Log file: ${logs.opts.file.filename}`)

    console.log(`\n`)
} else if (process.node.NODE_ENV == 'production') {
    log.info(`NODE_ENV: ${process.env.NODE_ENV}`)
}

log.info(`Starting server`)
app.listen(port, host)
log.info(`Server running in Docker on http://${host}:${port}`)